from flask import Flask, jsonify, request
from flaskext.mysql import MySQL
import json

app2 = Flask(__name__)

mysql = MySQL()

# MySQL configurations
app2.config['MYSQL_DATABASE_USER'] = 'root'
app2.config['MYSQL_DATABASE_PASSWORD'] = 'secret'
app2.config['MYSQL_DATABASE_DB'] = 'lol'
app2.config['MYSQL_DATABASE_HOST'] = 'mysql'

mysql.init_app(app2)

@app2.route('/')
def get():
    cur = mysql.connect().cursor()
    cur.execute('''select * from lol.lol_champion''')
    r = [dict((cur.description[i][0], value)
                for i, value in enumerate(row)) for row in cur.fetchall()]
    return jsonify({'myCollection2' : r})

@app2.route('/', methods=['POST'])
def post():
    conn = mysql.connect()
    cur = conn.cursor()
    details = request.form
    name = details['name']
    description = details['description']
    cur.execute("INSERT INTO lol.lol_champion(name, description) VALUES (%s, %s)", (name, description))
    conn.commit()
    cur.close()
    return 'Champion was created from app2'

if __name__ == '__main__':
   app2.run(debug=True, host='0.0.0.0')
