////////////////////////////////////////////////////////

To RUN this project you need to.

1. Run the command:
	docker-compose up  -d
<br>otherwise try:
	docker-compose up  --build

On project folder to start the Docker containers.

2. Be sure to have opened the ports on your web server:

	-8080 <br>
	-5000 <br>
	-3306

3. Once the project containers are setup, enter the MySql container,

	docker exec -it load-balancing_mysql_1 mysql  -p

and create the database:

	DROP SCHEMA

	IF EXISTS lol;
   	CREATE SCHEMA lol COLLATE = utf8_general_ci;

	USE lol;

	CREATE TABLE IF NOT EXISTS lol_champion(
    	champ_id INT AUTO_INCREMENT PRIMARY KEY,
    	name VARCHAR (50) NOT NULL,
    	description VARCHAR (450) NOT NULL
	);

	INSERT INTO lol_champion (name , description) VALUES
	("Lee Sin", "The Blind Monk"),
	("Gragas","The Rabble Rouser"),
	("Sylas","The Unshackled");

4. Go to Web Browser: IP_ADDRESS:8080

5. Be happy :).

AUTHORS: Juan Diego Gomez Gomez, Nicolas Javier Carreño Perea & Cristhian Steven Camargo Reyes